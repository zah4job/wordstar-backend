import pytest

from utils.fast_simple_build import matched


@pytest.mark.parametrize(
    ('long_word', 'word', 'result'),
    [
        ('абвгд', 'аб', True),
        ('абвгд', 'бв', True),
        ('абвгд', 'гд', True),
        ('аaбвгд', 'аб', True),
        ('абвгд', 'aa', False),
        ('абвгд', 'ад', True),
        ('абвгд', 'да', True),
    ]
)
def test_matched(long_word, word, result):
    actual_result = matched(long_word, word)

    assert result == actual_result
