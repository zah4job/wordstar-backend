"""
ASGI config for wordstar project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/howto/deployment/asgi/
"""

import os

import django

django.setup()
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.core.asgi import get_asgi_application

from wordstar.routing import websocket_urlpatterns

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'wordstar.settings')
asgi_app = get_asgi_application()
socket_app = AuthMiddlewareStack(URLRouter(websocket_urlpatterns))

application = ProtocolTypeRouter({
    "http": asgi_app,
    "websocket": socket_app
})
