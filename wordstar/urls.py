from django.contrib import admin
from django.urls import include, path
from rest_framework.routers import DefaultRouter

from main.views import GameViewSet, SoloGameViewSet, TurnViewSet
# api
from main.views.new_word import NewWordViewSet
from main.views.word_report import WordReportViewSet

api_router = DefaultRouter()
api_router.register('game', GameViewSet, 'game')
api_router.register('turn', TurnViewSet, 'turn')
api_router.register('solo', SoloGameViewSet, 'solo')
api_router.register('new-word', NewWordViewSet, 'new-word')
api_router.register('word-report', WordReportViewSet, 'word-report')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(api_router.urls)),
]
