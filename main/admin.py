import datetime
import os.path

from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponse
from django.urls import path

from known_words import known_words
from main.models import Game, NewWord, Turn, WordReport
from utils.actualize_bot_words import actualize_bot_words


@admin.register(Game)
class GameAdmin(ModelAdmin):
    """Админка для игры."""


@admin.register(Turn)
class TurnAdmin(ModelAdmin):
    """Шаги в игре."""

    search_fields = ('game',)


@admin.register(NewWord)
class NewWordAdmin(ModelAdmin):
    """Заявка на новое слово"""

    list_display = ('word', 'checked', 'satisfied', 'reporter', 'created_at')
    list_filter = ('checked', 'satisfied')
    actions = ('mark_as_good', 'mark_as_bad')

    @admin.action(description='Добавить в словарь')
    def mark_as_good(self, request, queryset):
        """Добавление слов в словарь игры."""
        queryset.update(checked=True, satisfied=True)

    @admin.action(description='Отклонить запрос')
    def mark_as_bad(self, request, queryset):
        """Отклонить запрос."""
        queryset.update(checked=True, satisfied=False)

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path(r'generate-dict/', generate_dict),
            path(r'replace-dict/', replace_dict),
        ]
        return my_urls + urls


@admin.register(WordReport)
class WordReportAdmin(ModelAdmin):
    """Заявка на удаление слова из игры."""
    list_display = ('word', 'checked', 'satisfied', 'created_at', 'reporter')
    list_filter = ('checked', 'satisfied')
    actions = ('mark_as_bad', 'mark_as_good')

    @admin.action(description='Удалить из словаря')
    def mark_as_bad(self, request, queryset):
        """Пометить элементы для удаления из словаря."""
        queryset.update(checked=True, satisfied=True)

    @admin.action(description='Оставить в игре')
    def mark_as_good(self, request, queryset):
        """Пометить слово как подходящее для игры."""
        queryset.update(checked=True, satisfied=False)


def build_word_lists():
    good_words = NewWord.objects.filter(checked=True, satisfied=True)
    good_words_set = set([word.word for word in good_words])
    new_words = good_words_set - known_words
    bad_words = WordReport.objects.filter(checked=True, satisfied=True)
    bad_words_set = set([word.word for word in bad_words])
    del_words = set(bad_words_set) & known_words
    return new_words, del_words


@staff_member_required
def generate_dict(request):
    new_words, del_words = build_word_lists()
    old_count = len(known_words)
    new_words_count = len(new_words)
    del_words_count = len(del_words)
    new_count = old_count + len(new_words) - len(del_words)

    msg = 'В словарь будут внесены следующие изменения:<br/>'
    msg += f'Будет слов: {old_count} + {new_words_count} - {del_words_count}'
    msg += f' = {new_count}<br/>'
    msg += '---------------добавим слова:-----------------<br/>'
    msg += '<ul>'
    msg += ''.join([f'<li>{word}</li>' for word in new_words])
    msg += '</ul><br/>'
    msg += '---------------удалим слова:-----------------<br/>'
    msg += '<ul>'
    msg += ''.join([f'<li>{word}</li>' for word in del_words])
    msg += '</ul>'
    msg += '<br/>Нажмите на ссылку, чтобы сгенерировать словарь'
    msg += '<a href="/admin/main/newword/replace-dict"> создать файл</a>'
    return HttpResponse(msg)


@staff_member_required
def replace_dict(request):
    new_words, del_words = build_word_lists()
    actual_dict = (known_words - del_words) | new_words
    actual_list = list(actual_dict)
    actual_list.sort()
    now = datetime.datetime.now()
    date_path = '{year}{month}{day}{hour}{min}{sec}.py'.format(
        year=now.year,
        month=now.month,
        day=now.day,
        hour=now.hour,
        min=now.minute,
        sec=now.second,
    )
    dict_path = f'known_words_{date_path}'
    with open(dict_path, 'wt', encoding='utf8') as dict_file:
        dict_file.write('known_words = {\n')
        lines_text = [f"\t'{word}',\n" for word in actual_list]
        dict_file.writelines(lines_text)
        dict_file.write('}')

    bot_dict_path = f'bot_words_{date_path}'
    actualize_bot_words(bot_dict_path)

    # full_path = os.path.abspath(dict_path)
    path_ul = f'<ul><li>{dict_path}</li><li>{bot_dict_path}</li></ul>'
    return HttpResponse(f'saved files: <br>{path_ul}')

