from rest_framework import mixins, status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from main.models import NewWord
from main.serializers import NewWordSerializer


class NewWordViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    GenericViewSet,
):
    """Заявка на добавление нового слова на сервер."""

    serializer_class = NewWordSerializer
    queryset = NewWord.objects.all()

    def create(self, request, *args, **kwargs):
        exists = NewWord.objects.filter(word=request.data['word']).count() > 0
        if exists:
            return Response(status=status.HTTP_208_ALREADY_REPORTED)
        return super().create(request, *args, **kwargs)
