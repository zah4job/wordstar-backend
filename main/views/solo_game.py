from django.core.exceptions import BadRequest
from rest_framework import mixins
from rest_framework.decorators import action
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from main.models import Game
from main.models.solo_game import SoloGame
from main.serializers.solo_game import SoloGameSerializer


class SoloGameViewSet(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    GenericViewSet,
):
    """Аpi для соло игры."""

    queryset = SoloGame.objects.all()
    serializer_class = SoloGameSerializer

    @action(
        methods=['POST'],
        url_path='game-over',
        detail=True,
    )
    def game_over(self, request: Request, *args, **kwargs) -> Response:
        """Окончание игры."""
        solo_game: SoloGame = self.get_object()
        solo_game.is_finished = True
        return Response('this is the end')
