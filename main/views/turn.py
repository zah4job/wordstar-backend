from rest_framework.viewsets import ModelViewSet

from main.models import Turn
from main.serializers import TurnSerializer


class TurnViewSet(ModelViewSet):
    """Шаги игроков."""

    serializer_class = TurnSerializer
    queryset = Turn.objects.all()
