from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from main.models import Game, Turn
from main.serializers import GameSerializer, TurnSerializer
from main.serializers.game import NewGameSerializer


class GameViewSet(ModelViewSet):
    """Игры."""

    serializer_class = GameSerializer

    def get_queryset(self):
        if self.action == 'list':
            return Game.objects.filter(is_started=False, is_finished=False)
        return Game.objects.all()

    @action(
        ('POST',),
        detail=False,
        url_path='new-game',
        serializer_class=NewGameSerializer
    )
    def new_game(self, request, *args, **kwargs):
        serializer: NewGameSerializer = self.get_serializer(data=request.data)
        serializer.is_valid(True)
        self.perform_create(serializer)

        return Response(f'{serializer.instance.pk}', status=201)

    @action(
        ('POST',),
        detail=True,
        url_path='make-turn',
        serializer_class=TurnSerializer,
    )
    def make_turn(self, request, *args, **kwargs):
        game = Game.objects.get(pk=kwargs['pk'])
        serializer: TurnSerializer = self.get_serializer(data={**request.data, 'game': game.pk})
        serializer.is_valid(True)
        turn = serializer.instance

        player_name = serializer.validated_data['author']
        is_owner = player_name == game.owner
        is_visitor = player_name == game.visitor

        if is_owner is False and is_visitor is False:
            raise ValidationError('это не твоя игра')

        # проверка того, что слово состоит биз букв исходного слова
        source_letters = list(game.source_word)
        word = serializer.validated_data['word'].lower()
        try:
            for letter in word:
                source_letters.remove(letter)
        except ValueError:
            raise ValidationError('слово не подходит')

        # проверка того, что это слово ещё не было названо в рамках игры
        try:
            Turn.objects.get(game=game, word=word)
        except Turn.DoesNotExist:
            self.perform_create(serializer)
        else:
            raise ValidationError('слово уже есть')

        return Response('ok', status=201)
