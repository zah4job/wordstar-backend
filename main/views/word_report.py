from django.core.exceptions import ValidationError
from rest_framework import mixins, status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from main.models import WordReport
from main.serializers.word_report import WordReportSerializer


class WordReportViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    GenericViewSet,
):
    """Заявка на удаление непотребного слова из базы."""

    serializer_class = WordReportSerializer
    queryset = WordReport.objects.all()

    def create(self, request, *args, **kwargs):
        exists = WordReport.objects.filter(word=request.data['word']).count() > 0
        if exists:
            return Response(status=status.HTTP_208_ALREADY_REPORTED)
        return super().create(request, *args, **kwargs)
