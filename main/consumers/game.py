import json

from channels.db import database_sync_to_async
from channels.exceptions import DenyConnection
from channels.generic.websocket import AsyncWebsocketConsumer

from main.models import Game


class GameConsumer(AsyncWebsocketConsumer):
    """Получение информации о ходе противника."""

    def __init__(self, *args, **kwargs):
        self.game_id = '0'
        self.group_name = 'unknown'
        self.game = None
        super().__init__(*args, **kwargs)

    async def connect(self):
        print('ws connect')
        self.game_id = self.scope['url_route']['kwargs']['game_id']
        self.group_name = f'Game_{self.game_id}'

        # if self.scope['user'] == AnonymousUser():
        #     raise DenyConnection("Такого пользователя не существует")

        await self.channel_layer.group_add(
            self.group_name,
            self.channel_name,
        )

        # If invalid game id then deny the connection.
        self.game = await get_name(self.game_id)
        if self.game is None:
            raise DenyConnection('wrong game id')

        await self.accept()

    async def receive(self, *args, **kwargs):
        print('receive')
        text_data = kwargs.get('text_data', {})
        source_message = json.loads(text_data)
        print(source_message)

        sender = source_message.get('sender')
        event = source_message.get('event')

        if sender and event:
            message = get_message(sender, event, source_message)
            message['sender'] = sender
            message['event'] = event
            message['type'] = 'game_event'
            await self.channel_layer.group_send(
                self.group_name,
                message,
            )

    # async def websocket_disconnect(self, message):
    #     await self.channel_layer.group_discard(
    #         self.group_name,
    #         self.channel_name,
    #     )

    async def game_event(self, message):
        # Send message to WebSocket
        bytes_message = json.dumps(message).encode('utf8')
        await self.send(bytes_data=bytes_message)


@database_sync_to_async
def get_name(game_id):
    try:
        game = Game.objects.get(pk=game_id)
    except Game.DoesNotExist:
        return None
    else:
        return game


def get_message(sender: str, event: str, source_message):
    """Формирование сообщения (словарь)."""
    if event == 'turn':
        return {
            'author': sender,
            'word': source_message['word'],
        }
    if event == 'gameover':
        return {
            'winner': source_message['winner'],
        }
