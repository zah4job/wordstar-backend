import json
import random

from channels.db import database_sync_to_async
from channels.exceptions import DenyConnection
from channels.generic.websocket import AsyncWebsocketConsumer

from main.models import Game


class LobbyConsumer(AsyncWebsocketConsumer):
    """Разговоры в лобби."""

    def __init__(self, *args, **kwargs):
        self.game_id = '0'
        self.group_name = 'unknown'
        self.game = None
        self.owner = ''
        self.visitor = ''
        super().__init__(*args, **kwargs)

    async def connect(self):
        """Подключение нового клиента."""
        print('lobby ws connect')
        self.game_id = self.scope['url_route']['kwargs']['game_id']
        self.group_name = f'Lobby_{self.game_id}'

        await self.channel_layer.group_add(
            self.group_name,
            self.channel_name,
        )

        # If invalid game id then deny the connection.
        self.game = await get_game(self.game_id)
        if self.game is None:
            raise DenyConnection('wrong game id')

        await self.accept()

    async def receive(self, *args, **kwargs):
        print('lobby consumer receive')
        text_data = kwargs.get('text_data', {})
        message = json.loads(text_data)
        print(message)
        sender = message.get('sender', '')
        event = message.get('event', '')

        await self.check_empty_lobby(sender, event)
        await self.check_members(sender, event)
        await self.check_started(sender, event)

        if sender and event:
            group_message = {
                'type': 'lobby_event',
                'game_id': self.game_id,
                'sender': sender,
                'event': event,
            }
            await self.channel_layer.group_send(
                self.group_name,
                group_message,
            )
        # self.prof.disable()
        # self.prof.print_statistics(sort_order='asc')

    # async def websocket_disconnect(self, message):
    #     with open('profile.txt', 'at') as file:
    #         file.write('3')
    #     await self.channel_layer.group_discard(
    #         self.group_name,
    #         self.channel_name,
    #     )
    #     with open('profile.txt', 'at') as file:
    #         file.write('4')

    async def lobby_event(self, group_message):
        # Send message to WebSocket
        bytes_message = json.dumps(group_message).encode('utf8')
        await self.send(bytes_data=bytes_message)

    async def check_empty_lobby(self, sender, event):
        """Проверка того, что комната осталась без хозяина. is_started=True - игра началась и не надо finish."""
        if event == 'leave':
            if self.game.owner == sender and self.game.is_started is False:
                await finish_game(self.game)

    async def check_members(self, sender, event):
        if event == 'joined':
            if self.game.owner != sender:
                await set_visitor(self.game, sender)

    async def check_started(self, sender, event):
        """Запуск игры (может делать только хост)."""
        if event == 'startgame':
            if self.game.owner == sender:
                await start_game(self.game)


@database_sync_to_async
def get_game(game_id):
    try:
        game = Game.objects.get(pk=game_id)
    except Game.DoesNotExist:
        return None
    else:
        return game


@database_sync_to_async
def finish_game(game: Game):
    game.is_finished = True
    game.save()


@database_sync_to_async
def set_visitor(game: Game, visitor: str):
    game.visitor = visitor
    game.save()


@database_sync_to_async
def start_game(game: Game):
    game.refresh_from_db()
    game.next_turn = random.choice([game.owner])  # , game.visitor])
    game.is_started = True
    game.save()
