# Generated by Django 4.0.3 on 2022-03-08 14:40

import django.core.validators
from django.db import migrations, models

import main.models.game
import main.models.turn


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_alter_game_owner_alter_game_visitor'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='owner',
            field=models.CharField(max_length=12, validators=[django.core.validators.RegexValidator('^[0-9a-zA-Zа-яА-Я]*$', 'Можно использовать только буквы и цифры.')], verbose_name='хост'),
        ),
        migrations.AlterField(
            model_name='game',
            name='source_word',
            field=models.CharField(default=main.models.game.random_source_word, max_length=30, verbose_name='слово'),
        ),
        migrations.AlterField(
            model_name='game',
            name='visitor',
            field=models.CharField(max_length=12, validators=[django.core.validators.RegexValidator('^[0-9a-zA-Zа-яА-Я]*$', 'Можно использовать только буквы и цифры.')], verbose_name='гость'),
        ),
        migrations.AlterField(
            model_name='turn',
            name='word',
            field=models.CharField(max_length=30, validators=[django.core.validators.RegexValidator('^[а-яА-Я]*$', 'Можно использовать только русские буквы.'), main.models.turn.word_is_known], verbose_name='слово'),
        ),
    ]
