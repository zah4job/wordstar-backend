from rest_framework.serializers import ModelSerializer

from main.models import Game


class GameSerializer(ModelSerializer):
    """Игра."""

    class Meta(object):
        model = Game
        fields = ('pk', 'owner', 'visitor', 'is_started', 'is_finished', 'created_at', 'source_word', 'next_turn',)


class NewGameSerializer(ModelSerializer):
    """Создание новой игры."""

    class Meta(object):
        model = Game
        fields = ('owner',)
