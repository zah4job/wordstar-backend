from rest_framework.serializers import ModelSerializer

from main.models import NewWord


class NewWordSerializer(ModelSerializer):
    """Заявка на добавление слова в базу."""

    class Meta(object):
        model = NewWord
        fields = ('created_at', 'reporter', 'word', 'checked', 'satisfied')
        read_only_field = ('created_at', 'checked', 'satisfied')
