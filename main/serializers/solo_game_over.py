from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from main.models import SoloGame


class SoloGameOverSerializer(mixins.UpdateModelMixin, GenericViewSet):
    """Сериализатор для завершения игры."""

    class Meta(object):
        model = SoloGame
        fields = ('is_win', 'user_answers', 'bot_answers',)
