from rest_framework.serializers import ModelSerializer

from main.models import Turn


class TurnSerializer(ModelSerializer):
    """Ход игрока."""

    class Meta(object):
        model = Turn
        fields = ('pk', 'game', 'author', 'word')
