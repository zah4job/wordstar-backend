from rest_framework.serializers import ModelSerializer

from main.models import WordReport


class WordReportSerializer(ModelSerializer):
    """Жалоба на слово. Удаление с сервера."""

    class Meta(object):
        read_only_fields = ('created_at',)
        model = WordReport
        fields = ('created_at', 'reporter', 'word')
