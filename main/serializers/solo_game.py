from rest_framework import serializers

from bot_words import bot_words
from main.models import SoloGame


class SoloGameSerializer(serializers.ModelSerializer):
    """Сериализатор для соло игры."""

    def get_all_answers_count(self: 'SoloGameSerializer', game: SoloGame):
        return len(bot_words[game.source_word])

    def get_all_answers(self: 'SoloGameSerializer', game: SoloGame):
        return bot_words[game.source_word]

    all_answers_count = serializers.SerializerMethodField()
    all_answers = serializers.SerializerMethodField()

    class Meta(object):
        model = SoloGame
        fields = (
            'pk', 'creator', 'is_started', 'is_finished', 'created_at',
            'source_word', 'is_win', 'all_answers_count', 'all_answers',
        )
        read_only_fields = (
            'pk', 'is_started', 'is_finished', 'created_at', 'source_word',
            'is_win',
        )
