import datetime

from django.db import models
from pytz import utc


def _utc_now():
    return datetime.datetime.utcnow().replace(tzinfo=utc)


class WordReport(models.Model):
    """Репорт плохого слова. Кандидат на удаление из базы."""

    reporter = models.CharField(
        'Заявитель',
        max_length=30,
        help_text='Имя игрока, который отправил жалобу',
    )
    word = models.CharField(
        'Слово',
        max_length=20,
        help_text='Слово-кандидат на удаление из базы',
        unique=True,
    )
    created_at = models.DateTimeField(
        'Создана',
        default=_utc_now,
        help_text='Время формирования жалобы',
    )
    checked = models.BooleanField(
        'Просмотрена',
        help_text='Жалоба просмотрена администратором',
        default=False,
        blank=True,
    )
    satisfied = models.BooleanField(
        'Удовлетворена',
        help_text='Жалоба удовлетворена (слово удалено из словаря)',
        default=False,
        blank=True,
    )

    def save(self, *args, **kwargs):
        """При сохранении - слово переводится в нижний регистр."""
        self.word = self.word.lower()
        return super().save(*args, **kwargs)
