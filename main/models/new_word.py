import datetime

from django.db import models
from pytz import utc


def _utc_now():
    return datetime.datetime.utcnow().replace(tzinfo=utc)


class NewWord(models.Model):
    """Заявка на добавление нового слова."""

    reporter = models.CharField(
        'Заявитель',
        max_length=30,
        help_text='Имя игрока, который отправил заявку',
    )
    word = models.CharField(
        'Слово',
        max_length=20,
        help_text='Слово для добавления на сервер',
        unique=True,
    )
    created_at = models.DateTimeField(
        'Создана',
        default=_utc_now,
        help_text='Время формирования заявки',
    )
    checked = models.BooleanField(
        'Просмотрена',
        help_text='Заявка просмотрена администратором',
        default=False,
        blank=True,
    )
    satisfied = models.BooleanField(
        'Удовлетворена',
        help_text='Заявка удовлетворена (слово добавлено)',
        default=False,
        blank=True,
    )

    def save(self, *args, **kwargs):
        """При сохранении - слово переводится в нижний регистр."""
        self.word = self.word.lower()
        return super().save(*args, **kwargs)
