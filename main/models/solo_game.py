import random
from datetime import datetime

import pytz
from django.core.validators import RegexValidator
from django.db import models

from bot_words import bot_words

username = RegexValidator(r'^[0-9a-zA-Zа-яА-Я]*$', 'Можно использовать только буквы и цифры.')


def random_source_word():
    """Случайное имя для игры (случайный выбор из сета)."""
    return random.choice(list(bot_words.keys()))


def now():
    return datetime.now(pytz.utc)


class SoloGame(models.Model):
    """Игра между двумя игроками."""

    creator = models.CharField(
        'хост', max_length=12, blank=False, validators=[username],
    )
    is_started = models.BooleanField('игра началась', default=True)
    is_finished = models.BooleanField('игра закончилась', default=False)
    created_at = models.DateTimeField('время создания игры', default=now)
    source_word = models.CharField(
        'слово', max_length=30, blank=False, default=random_source_word,
    )
    is_win = models.BooleanField(
        'игрок победил', blank=True, null=True, default=None,
    )
    user_answers = models.TextField(
        'ответы игрока',
        blank=True,
        help_text='Ответы игрока через точку с запятой',
    )

    class Meta(object):
        verbose_name = 'одиночная игра'
        verbose_name_plural = 'одиночные игры'

    def __str__(self):
        return f'Game#{self.pk}: {self.creator} - {self.source_word}'
