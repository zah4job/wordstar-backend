import random
from datetime import datetime

import pytz
from django.core.validators import RegexValidator
from django.db import models

from long_words import long_words

username = RegexValidator(r'^[0-9a-zA-Zа-яА-Я]*$', 'Можно использовать только буквы и цифры.')


def random_source_word():
    """Случайное имя для игры (случайный выбор из сета)."""
    return random.choice(list(long_words))


def now():
    return datetime.now(pytz.utc)


class Game(models.Model):
    """Игра между двумя игроками."""

    owner = models.CharField('хост', max_length=12, blank=False, validators=[username])
    visitor = models.CharField('гость', max_length=12, validators=[username])
    next_turn = models.CharField('следующий ход', max_length=12, validators=[username], default='')
    is_started = models.BooleanField('игра началась', default=False)
    is_finished = models.BooleanField('игра закончилась', default=False)
    created_at = models.DateTimeField('время создания игры', default=now)
    source_word = models.CharField('слово', max_length=30, blank=False, default=random_source_word)

    class Meta(object):
        verbose_name = 'игра'
        verbose_name_plural = 'игры'

    def __str__(self):
        return f'Game#{self.pk}: {self.owner} - {self.visitor}'
