from django.core.validators import RegexValidator
from django.db import models
from rest_framework.exceptions import ValidationError

from known_words import known_words
from main.models.game import Game

ru_letters = RegexValidator(r'^[а-яА-Я]*$', 'Можно использовать только русские буквы.')


def word_is_known(word: str) -> None:
    """Слово есть в словаре известных слов."""
    if word not in known_words:
        raise ValidationError('такого слова нет')


class Turn(models.Model):
    """Один ход игрока."""
    game = models.ForeignKey(Game, on_delete=models.CASCADE, blank=False)
    author = models.CharField('игрок', max_length=12, blank=False)
    word = models.CharField('слово', max_length=30, blank=False, validators=[ru_letters, word_is_known])

    class Meta(object):
        verbose_name = 'ход игрока'
        verbose_name_plural = 'ходы игроков'

    def __str__(self):
        return f'{self.game}: {self.author}: {self.word}'
