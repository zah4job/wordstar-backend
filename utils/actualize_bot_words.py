from bot_words import bot_words
from known_words import known_words
from loguru import logger as log


def actualize_bot_words(file_path: str) -> None:
    """Удалить из списка bot_words слова, которых нет в known_words."""

    for long_word, variants in bot_words.items():
        actual_words = set(variants) & known_words
        log.info(f'{len(variants)} -> {len(actual_words)}')
        bot_words[long_word] = actual_words

    with open(file_path, 'wt', encoding='utf8') as bot_file:
        bot_file.write('bot_words = {\n')
        for long_word, variants in bot_words.items():
            variant_list = list(variants)
            variant_list.sort()
            bot_file.write(f"\t'{long_word}': {tuple(variant_list)},\n")
        bot_file.write('}\n')


if __name__ == '__main__':
    actualize_bot_words('new_bot_words.py')
