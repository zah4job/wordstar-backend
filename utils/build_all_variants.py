import time
from known_words import known_words
# from long_words import long_words
from bot_words import bot_words
from itertools import permutations, combinations


def word_answers(word):
    variants = len(word) - 2
    res = []
    checked = []

    for variant_index in range(variants):
        for letter_set in combinations(word, min(variant_index + 3, 5)):
            letter_set = list(letter_set)
            letter_set.sort()
            if letter_set in checked:
                continue
            for word_letters in permutations(letter_set):
                mb_word = ''.join(word_letters)
                if mb_word not in res and mb_word in known_words:
                    res.append(mb_word)
    res.sort()
    return res


if __name__ == '__main__':
    """Построение списка всех возможных слов, составленных из long_words.
    
    Используются перестановки. Длина слов ограничена.
    """
    start = time.perf_counter()
    answers = {}
    print(2)

    total = len(bot_words.keys())
    for i, known_word in enumerate(list(bot_words.keys())[3476:]):
        with open('founded_words.txt', 'at', encoding='utf8') as file:
            start_iteration = time.perf_counter()
            percent = float(i + 1) / float(total) * 100.0
            print(f'index: {i}/{total} ({percent:0.3f}%): {known_word}')
            to_save = word_answers(known_word)
            str_tuple = "', '".join(to_save)
            str_tuple = "('" + str_tuple + "',),\n"
            file.write(f"'{known_word}': {str_tuple}")
            spend = time.perf_counter() - start_iteration
            print(f'{known_word}: {len(to_save)} in {spend:0.3f} sec')

    print(f'end with: {time.perf_counter() - start:0.3f} sec')
