from typing import List

from known_words import known_words
from long_words import long_words
from loguru import logger


def matched(long_word: str, word: str) -> bool:
    matches = False
    if all([letter in long_word for letter in word]):
        # полная проверка
        long_word_copy = [letter for letter in long_word]
        matches = True
        for letter in word:
            try:
                long_word_copy.remove(letter)
            except ValueError:
                matches = False
                break
    return matches


def matched_words(long_word:str) -> List[str]:
    words = []
    for word in known_words:
        if matched(long_word, word):
            words.append(word)
    words.sort()
    return words


def find_all_words():
    long_list = list(long_words)
    matches = dict()
    long_list.sort()
    total = len(long_words)
    for index, long_word in enumerate(long_list):
        matches[long_word] = matched_words(long_word)
        percent = index / total * 100
        logger.info(f'index: {index}/{total}, {percent:.2f}%')
        logger.info(f'{len(matches[long_word])} from {long_word}')
        with open('long_list.txt', 'at', encoding='utf8') as long_file:
            ans_tuple = tuple(matches[long_word])
            long_file.write(f"\t'{long_word}': {ans_tuple},\n")

    return matches


if __name__ == '__main__':
    """Построение словаря (большое)->Tuple(маленькие).
    
    всех слов "маленькие", составленных из "большого"
    """
    all_words = find_all_words()
    logger.info('all done')
